package br.com.senac.triagulo;

public class Triangulo {

    public static final String EQUILATERO = "Equilátero";
    public static final String ESCALENO = "Escaleno";
    public static final String ISOCELES = "Isóceles";
    public static final String NAO_E_TRIANGULO = "Não é Triangulo";
    
    private double ladoA;
    private double ladoB;
    private double ladoC;

    public Triangulo() {
    }

    public Triangulo(double ladoA, double ladoB, double ladoC) {
        this.ladoA = ladoA;
        this.ladoB = ladoB;
        this.ladoC = ladoC;
    }

    public boolean isTriangulo() {
        return this.ladoA < this.ladoB + this.ladoC
                && this.ladoB < this.ladoA + this.ladoC
                && this.ladoC < this.ladoA + this.ladoB;

    }

    public boolean isEquilatero() {
        if (this.isTriangulo()) {
            return ((this.ladoA == this.ladoB)
                    && (this.ladoA == this.ladoC));
        } else {
            return false;
        }
    }

    public boolean isEscaleno() {
        if (this.isTriangulo()) {
            return ((this.ladoA != this.ladoB)
                    && (this.ladoA != this.ladoC));
        } else {
            return false;
        }
    }

    public boolean isIsoceles() {
        if (this.isTriangulo()) {
            return ((this.ladoA == this.ladoB) && (this.ladoA != this.ladoC))
                    || ((this.ladoA == this.ladoC) && (this.ladoA != this.ladoB))
                    || ((this.ladoB == this.ladoC) && (this.ladoB != this.ladoA));
        } else {
            return false;
        }
    }
    
    
    public String getClassificacao(){
        if(this.isTriangulo()){
            if(this.isEquilatero()){
                return Triangulo.EQUILATERO ; 
            }else if(this.isEscaleno()){
                return Triangulo.ESCALENO ; 
            }else{
                return Triangulo.ISOCELES ; 
            }
        }else{
            return Triangulo.NAO_E_TRIANGULO ; 
        }
    }

    public double getLadoA() {
        return ladoA;
    }

    public void setLadoA(double ladoA) {
        this.ladoA = ladoA;
    }

    public double getLadoB() {
        return ladoB;
    }

    public void setLadoB(double ladoB) {
        this.ladoB = ladoB;
    }

    public double getLadoC() {
        return ladoC;
    }

    public void setLadoC(double ladoC) {
        this.ladoC = ladoC;
    }

}
