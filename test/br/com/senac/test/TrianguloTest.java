/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.triagulo.Triangulo;
import org.junit.Test;
import static org.junit.Assert.*;

public class TrianguloTest {

    public TrianguloTest() {
    }

    @Test
    public void existeTriangulo() {
        Triangulo triangulo = new Triangulo(10, 10, 10);
        assertTrue(triangulo.isTriangulo());
    }

    @Test
    public void naoExisteTriangulo() {
        Triangulo triangulo = new Triangulo(1, 1000000, 1);
        assertFalse(triangulo.isTriangulo());
    }

    @Test
    public void ehTrianguloEquilateroDeveSerEquilatero() {
        Triangulo triangulo = new Triangulo(10, 10, 10);

        assertTrue(triangulo.isEquilatero());

    }

    @Test
    public void ehTrianguloEscalenoNaoDeveSerEquilatero() {
        Triangulo triangulo = new Triangulo(10, 11, 12);

        assertFalse(triangulo.isEquilatero());

    }

    @Test
    public void ehTrianguloIsocelesNaoDeveSerEquilatero() {
        Triangulo triangulo = new Triangulo(10, 10, 12);

        assertFalse(triangulo.isEquilatero());

    }

    @Test
    public void ehTrianguloEscalenoDeveSerEscaleno() {
        Triangulo triangulo = new Triangulo(10, 11, 12);

        assertTrue(triangulo.isEscaleno());

    }

    @Test
    public void ehTrianguloEquilateroNaoDeveSerEscaleno() {
        Triangulo triangulo = new Triangulo(10, 10, 10);

        assertFalse(triangulo.isEscaleno());

    }

    @Test
    public void ehTrianguloIsocelesNaoDeveSerEscaleno() {
        Triangulo triangulo = new Triangulo(10, 10, 11);

        assertFalse(triangulo.isEscaleno());

    }

    @Test
    public void ehTrianguloIsocelesDeveSerIsoceles() {
        Triangulo triangulo = new Triangulo(10, 10, 11);

        assertTrue(triangulo.isIsoceles());

    }

    @Test
    public void ehTrianguloEquilateroNaoDeveSerIsoceles() {
        Triangulo triangulo = new Triangulo(10, 10, 10);

        assertFalse(triangulo.isIsoceles());

    }

    @Test
    public void ehTrianguloEscalenoNaoDeveSerIsoceles() {
        Triangulo triangulo = new Triangulo(10, 11, 12);

        assertFalse(triangulo.isIsoceles());

    }

    @Test
    public void naoEhTrianguloClassificaIsoceles() {
        Triangulo triangulo = new Triangulo(1, 1, 1000000);

        assertFalse(triangulo.isIsoceles());
    }

    @Test
    public void naoEhTrianguloClassificaEscaleno() {
        Triangulo triangulo = new Triangulo(1, 2, 1000000);

        assertFalse(triangulo.isEscaleno());
    }

    @Test
    public void equilateroDeveSerClassificadoComoEquilatero() {
        Triangulo triangulo = new Triangulo(10, 10, 10);

        assertEquals(Triangulo.EQUILATERO, triangulo.getClassificacao());
    }

    @Test
    public void isocelesNaoDeveSerClassificadoComoEquilatero() {
        Triangulo triangulo = new Triangulo(10, 10, 15);

        assertNotEquals(Triangulo.EQUILATERO, triangulo.getClassificacao());
    }

    @Test
    public void escalenoNaoDeveSerClassificadoComoEquilatero() {
        Triangulo triangulo = new Triangulo(10, 11, 15);

        assertNotEquals(Triangulo.EQUILATERO, triangulo.getClassificacao());
    }

    @Test
    public void isocelesDeveSerClassificadoComoIsoceles() {
        Triangulo triangulo = new Triangulo(10, 10, 15);

        assertEquals(Triangulo.ISOCELES, triangulo.getClassificacao());
    }

    @Test
    public void equilateroNaoDeveSerClassificadoComoIsoceles() {
        Triangulo triangulo = new Triangulo(10, 10, 10);

        assertNotEquals(Triangulo.ISOCELES, triangulo.getClassificacao());
    }

    @Test
    public void escalenoNaoDeveSerClassificadoComoIsoceles() {
        Triangulo triangulo = new Triangulo(10, 11, 15);

        assertNotEquals(Triangulo.ISOCELES, triangulo.getClassificacao());
    }

    @Test
    public void escalenoDeveSerClassificadoComoEscaleno() {
        Triangulo triangulo = new Triangulo(10, 11, 15);

        assertEquals(Triangulo.ESCALENO, triangulo.getClassificacao());
    }

    @Test
    public void equilateroNaoDeveSerClassificadoComoEscaleno() {
        Triangulo triangulo = new Triangulo(10, 10, 10);

        assertNotEquals(Triangulo.ESCALENO, triangulo.getClassificacao());
    }

    @Test
    public void isocelesNaoDeveSerClassificadoComoEscaleno() {
        Triangulo triangulo = new Triangulo(10, 10, 15);

        assertNotEquals(Triangulo.ESCALENO, triangulo.getClassificacao());
    }

    @Test
    public void naoTrianguloNaoDeveSerClassificadoComoEquilatero() {
        Triangulo triangulo = new Triangulo(1, 1, 1000);

        assertEquals(Triangulo.NAO_E_TRIANGULO, triangulo.getClassificacao());
    }

    @Test
    public void naoTrianguloNaoDeveSerClassificadoComoIsoceles() {
        Triangulo triangulo = new Triangulo(1, 1, 1000);

        assertEquals(Triangulo.NAO_E_TRIANGULO, triangulo.getClassificacao());
    }

    @Test
    public void naoTrianguloNaoDeveSerClassificadoComoEscaleno() {
        Triangulo triangulo = new Triangulo(1, 1, 1000);

        assertEquals(Triangulo.NAO_E_TRIANGULO, triangulo.getClassificacao());
    }

}
